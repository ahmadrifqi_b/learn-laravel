@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between">
            {{--  --}}
            <div>
                @if (isset($category)) {{--or @isset() ... @else ... @endisset --}}
                    <h4>Category: {{ $category->name }}</h4>
                @endif

                @if (isset($tag))
                    <h4>Tag: {{ $tag->name }}</h4>
                @endif

                @if(!isset($category) && !isset($tag))
                    <h4>All Post</h4>
                @endif
            </div>

            {{-- New Post --}}
            <div>
                @if(Auth::check())
                    <a href="{{ route('posts.create') }}" class="btn btn-primary">New Post</a>
                @else
                    <a href="{{ route('login') }}" class="btn btn-primary">Login to create new Post</a>
                @endif
            </div>

        </div>
        <div>
            @include('alert')
        </div>

        <hr>
        <div class="row">
             {{-- <div class="col-md-7"> 7 --}}
                @if ($posts->count())
                    @foreach ($posts as $post)
                        <div class="col-md-4">
                            <div class="card mb-4">
                                <div class="card-header">
                                    {{ $post -> title }}
                                    <div class="text-secondary">
                                        Category : "<a href="/categories/{{ $post->category->name }}">{{ $post->category->name }}</a>"
                                    </div>
                                </div>
                                @if ($post->thumbnail)
                                    <a href="{{ route('posts.show', $post->slug) }}">
                                        <img style="height: 220px;object-fit: cover;object-position: center;"class="card-img-top" src="{{ $post->takeImage }}" {{-- alt="" --}}>
                                    </a>
                                @endif
                                {{--  asset($post->takeImage()) if not using getIndexAttribute on Model --}}
                                <div class="card-body">
                                    <div>
                                        <a href="{{ route('categories.show', $post->category->slug) }}" class="text-secondary">
                                            <small>
                                                {{ $post->category->name }} -
                                            </small>
                                        </a>
                                        @foreach ($post->tags as $tag)
                                            <a href="{{ route('tags.show', $tag->slug) }}" class="text-secondary">
                                                <small>
                                                    {{ $tag->name }}
                                                </small>
                                            </a>
                                        @endforeach
                                    </div>

                                    <h5>
                                        <a class="text-dark" href="{{ route('posts.show', $post->slug) }}" class="card-title">
                                            {{ $post->title }}
                                        </a>
                                    </h5>

                                    <div class="text-secondary my-3">
                                        {{ Str::limit( $post -> body , 150 , '.') }}
                                    </div>

                                    {{-- <a href="/posts/{{ $post->slug }}">Read More</a> --}}
                                    <div class="d-flex justify-content-between align-items-center mt-2">
                                        <div class="media align-items-center">
                                            <img width="40" class="rounded-circle mr-3" src="{{ $post->author->gravatar() }}" alt="">
                                            <div class="media-body">
                                                <div>
                                                    {{ $post->author->name }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-secondary">
                                            <small>
                                                Published on {{ $post->created_at->format("d F Y")}}
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                    {{-- card-footer d-flex justify-content-between --}}
                                    {{-- Published on {{ $post->created_at->format("d M, Y")}} or d m, y or d F, Y
                                    and etc diffForHumans() -> change language on app/config/app.php/locale--}}

                                    {{-- @if (auth()->user()->is($post->author))
                                        @include('posts.partials.delete-control')
                                    @endif --}}
                                    {{-- <form action="/posts/{{ $post->slug }}/delete" method="post">
                                        @csrf
                                        @method("DELETE")
                                        <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                                    </form> --}}
                                    {{-- @auth --}}
                                    {{-- @if (auth()->user()->is($post->author)) --}}

                                    {{-- @endif --}}
                                    {{-- @endauth --}}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-info">
                        There's no post.
                    </div>
                @endif
            </div>
        </div>



            {{-- or
                looping using
                @forelse($posts as $post)
                    there is
                @empty
                    there is no
                @endforelse--}}

        {{-- <div class="d-flex justify-content-center">--}}
            <div>

                {{-- look at view -> vendor -> etc. show by default bootstrap-4 --}}
                {{-- for global to AppServiceProvider.php --}}
                {{ $posts->links('pagination::bootstrap-4') }}

            </div>
        {{-- </div> --}}

    </div>
@endsection
