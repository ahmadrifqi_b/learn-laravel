<!-- Button trigger modal -->
<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal">
    Delete
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Anda yakin ingin menghapusnya?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div>{{ $post->title }}</div>
                    <div class="text-secondary"><small> {{ $post->created_at->format("d F, Y") }}</small></div>
                </div>
            </div>
            <div class="modal-footer">
                <form action="/posts/{{ $post->slug }}/delete" method="post">
                    @csrf
                    @method("DELETE")
                    <div class="d-flex">

                    </div>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Tidak</button>
                    <button class="btn btn-danger" type="submit">Ya</button>
                </form>
            </div>
        </div>
    </div>
</div>
