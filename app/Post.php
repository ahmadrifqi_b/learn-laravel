<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //protected $table = 'post'; no default

    /*public function getRouteKeyName()
    {
        return 'slug';
    }*/

    //php artisan tinker => Post::latestPFirst;
    // public function scopeLatestFirst()
    // {
    //     return $this->latest()->first();
    // }

    protected $fillable = ['title', 'slug', 'body', 'category_id', 'thumbnail'];
    //protected $guarded = []; only for you, not users.

    protected $with = ['author', 'category', 'tags'];


    //relation database
    public function category()
    {
        //return $this->hasOne(Category::class);
        return $this->belongsTo(Category::class);

        //return $this->belongsTo(Category::class, 'category_id'); is default column table
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    // public function takeImage()
    public function getTakeImageAttribute()
    {
        return "/storage/" . $this->thumbnail;
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
