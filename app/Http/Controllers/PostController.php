<?php

namespace App\Http\Controllers;
use App\{Post, Category, Tag};
// use App\Category;
// use App\Tag;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except([
            'index',
            'show',
        ]);
    }

    public function show(Post $post) //$slug
    {
        //$post = \DB::table('posts')->where('slug', $slug)->first();
        //$post = Post::where('slug', $slug)->firstOrFail();

        //no using back slash \App\
        //dd($post);

        /*if(is_null($post))
        if(!$post){
            abort(404);
        }

        change OrFail*/

        //return view('posts.show', compact('slug'));

        $posts = Post::where('category_id', $post->category_id)->latest()->limit(6)->get();
        //with('author', 'tags', 'category')->
        return view('posts.show', compact('post', 'posts'));
    }

    public function index()
    {
        //return Post::get(['title', 'slug']);

        //Limit function => limit('total')->get(); // take('total')->get(); // paginate('total');;
        // and simplePaginate('total');

        //$posts = Post::paginate(10);
        //return view('posts.index', ['posts' => $posts]);

        //return Post::with('author', 'tags', 'category')->latest()->get();

        return view('posts.index', [
            'posts' => Post::latest()->paginate(9),
            //with('author', 'tags', 'category')->
        ]);
        //paginate / get / etc
    }

    public function create()
    {
        return view ('posts.create', [
            'post' => new Post(),
            'categories' => Category::get(),
            'tags' => Tag::get(),
        ]);
    }

    public function store(PostRequest $request)
    //(Request $request)
    {
        //dd(request('tags'));   -> tested array tags
        // dd('posted');
        // $post = new Post;
        // $post->title = $request->title;
        // $post->slug = \Str::slug($request->title);
        // $post->body = $request->body;
        // $post->save();

        // using "public function (Request $request)"
        // $this->validate($request, [
        //     'title'=>'required|min:3|max:50',
        //     'body'=>'required',
        // ]);
        // $post = $request->all();

        /* or
        $request->validate([
            etc
        ]);
        */
        //dd(auth()->user());
        //dd(request()->file('$thumbnail));

        $request->validate([
            'thumbnail' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $post = $request->all();
        $slug = \Str::slug($request->title);
        $post['slug'] = $slug;

        if (request()->file('thumbnail')) {
            $thumbnail = request()->file('thumbnail')->store("images/posts");
        } else {
            $thumbnail = null;
        } //or $thumbnail = request()->file('thumbnail') ? request()->file('thumbnail')->store("images/posts") : null;

        //$thumbnail = request()->file('thumbnail');
        // $thumbnailUrl = $thumbnail->storeAs("images/posts", "{$slug}.{$thumbnail->extension()}");
       // $thumbnailUrl = $thumbnail->store("images/posts"); //this is shortcut

        $post['category_id'] = request('category');
        $post['thumbnail'] = $thumbnail;
        //$thumbnailUrl;

        //Create new post
        $post = auth()->user()->posts()->create(
            $post
            // [
            //     'title' => $request->title,
            //     'slug' => \Str::slug('$request->title'),
            //     'body' => $request->body,
            // ]
        );
        $post->tags()->attach(request('tags'));

        session()->flash('success', 'The post was created');

        return redirect()->to('posts');

        //return back(); to same page

        //dd($request->all()); for see it
    }

    public function edit(Post $post)
    {
        return view('posts.edit', [
            'post' => $post,
            'categories' => Category::get(),
            'tags' => Tag::get(),
        ]
        // compact('post')
        );
    }

    public function update(PostRequest $request, Post $post)
    {
        // dd('updated');
        //dd($post);

        $request->validate([
            'thumbnail' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $this->authorize('update', $post);
        $attr = $request->all();

        if (request()->file('thumbnail')){
            \Storage::delete($post->thumbnail);
            $thumbnail = request()->file('thumbnail')->store("images/posts");
        } else {
            // $thumbnail = request()->file('thumbnail')->store("images/posts"); //same from create super simple
            $thumbnail = $post->thumbnail;
        }

        // $this->requestValidate();
        $attr['category_id']=request('category');
        $attr['thumbnail']=$thumbnail;
        $post->update($attr);
        $post->tags()->sync(request('tags'));

        session()->flash('success', 'The post was updated');
        return redirect('posts');
        //return back();
    }

    public function destroy(Post $post)
    {
        //dd($post);

        //using policy
        $this->authorize('delete', $post);
        \Storage::delete($post->thumbnail);
        $post->tags()->detach();
        $post->delete();
        session()->flash('success', 'The post was deleted');
        return redirect('posts');

        //no use policy
        // if (auth()->user()->is($post->author)) {
        //     //dd('ya benar');
        //     $post->tags()->detach();
        //     $post->delete();

        //     session()->flash('success', 'The post was destroyed');
        //     return redirect('posts');
        // } else {
        //     session()->flash("error", "It wasn't your post");
        //     return redirect('posts');
        // }
    }

    // public function requestValidate()
    // {
    //     return request()->validate([
    //         'title'=>'required|min:3|max:50',
    //         'body'=>'required',
    //     ]);
    // }
}
