<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class SearchController extends Controller
{
    public function post()
    {
        // return request('query');

        $query = request('query');

        $posts = Post::where("title", "like", "%$query%")->latest()->paginate(10);
        // return $posts;
        return view('posts.index', compact('posts'));
    }
}
