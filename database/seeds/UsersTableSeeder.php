<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\User::Create([
            'name' => 'Ahmad Rifqi Baidhowi',
            'username' => 'ahmadrifqib',
            'password' => bcrypt('lamongan'),
            'email' => 'ahmad.rifqi@outlook.co.id',
        ]);
    }
}
