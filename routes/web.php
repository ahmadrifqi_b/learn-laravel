<?php

//Class name

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
//use Illuminate\Http\Request;


//Route::get('/', 'HomeController@home');
//Route::get('/', 'HomeController');                change by Auth::routes

//Route::get('posts/{slug}', 'PostController@show');

Route::get('search', 'SearchController@post')->name('search.posts');

//For Group Post, exambple Route::prefix('blog')->middleware('auth')->group(function(){ then link is local/blog/posts/create
Route::middleware('auth')->group(function(){
    Route::get('posts', 'PostController@index')->name('posts.index')->withoutMiddleware('auth');

    Route::get('posts/create', 'PostController@create')->name('posts.create');
    Route::post('posts/create/store', 'PostController@store');

    Route::get('posts/{post:slug}/edit', 'PostController@edit');
    Route::patch('posts/{post:slug}/edit', 'PostController@update');
    //put keseluruhan and patch parsial

    Route::delete('posts/{post:slug}/delete', 'PostController@destroy');
});


Route::get('categories/{category:slug}', 'CategoryController@show')->name('categories.show');
Route::get('tags/{tag:slug}', 'TagController@show')->name('tags.show');

Route::get('posts/{post:slug}', 'PostController@show')->name('posts.show');



//Route::view('home', 'home');
Route::view('contact', 'contact');
Route::view('about', 'about');




/*Solo Learn

Route::get('/', function () {
    return view('welcome');
});


Route::get('url', 'view');



with class name
Route::get('contact', function (Request $request){
    //return $request->path() == 'contact' ? true : false;
    return $request->path() == 'contact' ? 'Benar' : 'Salah';
});

//without class name A.k.a request function
Route::get('contact', function(){
    //return request()->fullUrl();
    //return request()->path();         "Setelah Domain"
    //return request()->is('contact') ? true : false;
});*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
